/*  TP - Tableur */

var tableur = {

    // Référence du tableau courant
    tableauCourant : {},
    matable: {},
    // Liste des Tabbleaux
    tableaux: [],

    init: function (name,nbcol,nbrow) {
        // Initialise le tableau avec des valeurs par défaut
        tableur.tableauCourant['nom']=name;
        tableur.tableauCourant['id']=1;
        tableur.tableauCourant['cellules']={};
        
        // On récupère la référence sur la div target
        let target = document.getElementById('target');
        tableur.matable = document.createElement('table');
        tableur.matable.setAttribute('class','table table-bordered');
        target.appendChild(tableur.matable);

        // On récupère une référence sur l'objet table de la page html.
        //var matable = document.getElementById(tablename);
        // On récupère les attributs de la table
        // let nbcol = matable.getAttribute("nbcol");
        // let nbrow = matable.getAttribute("nbrow");
        //console.log("nbcol="+nbcol+",nbrow="+nbrow);
        let entetes = tableur.matable.insertRow();
        // Ajoute une cellule vide pour une question d'alignement des colonnes
        let header = document.createElement('th');
        entetes.appendChild(header);
        // Une boucle sur les entetes 
        for(col=0;col<nbcol; col++ ) {
            let header = document.createElement('th');
            header.innerHTML = String.fromCharCode(65+col);
            entetes.appendChild(header);
        }
        // Les entête de colonnes
        // On boucle sur toutes les lignes et sur toutes les colonnes
        for(ligne=0;ligne<nbrow; ligne++ ) {

            let row = tableur.matable.insertRow();

            let header = document.createElement('th');
            header.innerHTML = ligne+1;
            row.appendChild(header);

            for(col=0;col<nbcol; col++ ) {
                // A chaque tour on créer une cellule avec ses attributs
               let cell = row.insertCell();
               let id = String.fromCharCode(65+col)+(ligne+1);
               cell.innerHTML = "";
               cell.id = id;
               cell.value = "";
               //cell.setAttribute('class','col-6');
               // On met la référence sur la fonction clickOnCell dans la propriété onclick de la cellule
               cell.onclick = tableur.clickOnCell; 
            }
        }
    }
,
    newTableau: function() {
        console.log("newTableau");
        let target = document.getElementById('target');
        target.innerHTML="";
        let label = document.createElement('label');
        let input = document.createElement('input');
        let button = document.createElement('button');
        input.setAttribute('type','text');
        input.setAttribute('id','nom');
        input.setAttribute('value','');
        input.setAttribute('placeHolder','nom du tableau');
        label.setAttribute('for','nom');
        label.setAttribute('value','Nom du tableau :');
        button.innerHTML="Enregistrer"
        button.onclick = function () {
            tableur.tableauCourant = {};
            tableur.tableauCourant['nom']=input.value;
            tableur.tableauCourant['id']=0;
            tableur.tableauCourant.cellules = {};
            // On garde le nom du tableau dans la liste des tableaux
            tableur.tableaux.push(tableur.tableauCourant['nom']);
            // On stocke la liste des tableaux dans le localStorage
            let storage = window.localStorage;
            storage.setItem('tableaux',JSON.stringify(tableur.tableaux));
            // Initialiser le tableau
            target.innerHTML="";
            tableur.init(tableur.tableauCourant['nom'],5,8);
        }
        target.appendChild(input);
        target.appendChild(label);
        target.appendChild(button);
    } 
    ,
    // Cette méthode est appelée lorsqu'on clique sur une cellule
    clickOnCell: function() {
        console.log("clickclick sur "+this.id);
        this.onclick="";
        let input = document.createElement('input');
        input.setAttribute('type','text');
        // Ajoute l'id de la cellule dans le champ input
        input.setAttribute('idCell',this.id);
        // Récupère la valeur de la cellule
        let value = this.value;
        input.setAttribute('value',value);
        // On met la valeur de l'id de la cellule dans l'input
        input.setAttribute('id','input-'+this.id);
        this.innerHTML="";
        this.appendChild(input);
        input.focus();
        input.onblur = tableur.closeInput;
    }
    ,
    closeInput: function() {
        console.log("closeInput de "+this.id);
        let input = document.getElementById(this.id);
  
        let idCell = input.getAttribute('idCell');
        console.log("idCell ="+idCell);
        // récupérer la référence sur la cellule
        let cell = document.getElementById(idCell);
        // On conserve la valeur dans la cellule
        cell.value=input.value;
        // On supprime l'objet input de la cellule
        cell.removeChild(input);
        // On replace la value dans la cellule
        cell.innerHTML=cell.value;
        cell.onclick=tableur.clickOnCell;
        tableur.persistCell(cell);
    }
    ,
    persistCell: function(obj) {
        let cell = {
            'id': obj.id,
            'value': obj.value,
        }
        // On empile les objets cell dans le tableau tableauCourant['cellule']
        if(obj.value=="") {
             delete tableur.tableauCourant.cellules[obj.id];
          }
           else {
               tableur.tableauCourant.cellules[obj.id]=cell;
            }
        
        console.log(JSON.stringify(tableur.tableauCourant));
    }
    ,
    persisteTableau: function(){
        // On récupère une référence sur le localStorage
        let storage = window.localStorage;
        storage.setItem(tableur.tableauCourant.nom,JSON.stringify(tableur.tableauCourant));
    }
    ,
    loadTableau: function(nom) {
        let storage = window.localStorage;
        // Transforme la chaine de caractères en objet javascript
        tableur.tableauCourant = JSON.parse(storage.getItem(nom));
        // Parcourir tous les éléments du tableau.

        for (key in tableur.tableauCourant.cellules ) {
            console.log(key+", value="+tableur.tableauCourant.cellules[key].value);
            let cell = document.getElementById(key);
            cell.innerHTML = tableur.tableauCourant.cellules[key].value;

        }

    }
    ,
    showTableau: function() {

    }

}
