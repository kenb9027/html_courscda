function click_on_add(){
    console.log("click on add");
    let storage = window.localStorage;
    storage.setItem('element1',"un");
}
function click_on_getAll(){
    console.log("click on getAll");
    let storage = window.localStorage;
    let str = storage.getItem('element1');
    console.log("element1 = "+str);
}
/**
 * Une fonction pour détecter si localStorage est supporté
 * @param {*} type 
 * @returns 
 */
function storageAvailable(type) {
    try {
        var storage = window[type],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            storage.length !== 0;
    }
}
// if (storageAvailable('localStorage')) {
// 	// Nous pouvons utiliser localStorage
//     console.log("localStorage OK");
// }
// else {
// 	// Malheureusement, localStorage n'est pas disponible
//     console.log("localStorage inutilisable");
// }