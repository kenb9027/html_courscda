/*  TP - Tableur */

var tableur = {

    // Référence du tableau courant
    tableauCourant : {},
    matable: {},
    // Liste des Tabbleaux
    tableaux: [],

    init: function (name,nbcol,nbrow) {
        // Initialise le tableau avec des valeurs par défaut
        tableur.tableauCourant['nom']=name;
        tableur.tableauCourant['id']=1;
        tableur.tableauCourant['cellules']={};

        // Initialise le gestionnaire d'événement du click sur la dropdown du menu
        let dropdown = document.getElementById('offcanvasNavbarDropdown');
        dropdown.onclick = function() {
        // On modifie la liste des tableaux dans la dropdown du menu
        
        let ul = document.getElementById('target4tableaux');
        ul.innerHTML='';
        tableur.tableaux.forEach(function (item) {
           console.log('item='+item);
           // <li><a class="dropdown-item" href="#">Action</a></li>
           let li = document.createElement('li');
           li.setAttribute('class','dropdown-item');
           li.innerHTML=item;
           li.onclick = function() {
              tableur.loadTableau(item);
              let span = document.getElementById('nameTableau');
              span.innerHTML=item;
              let storage = window.localStorage;
              // On mémorise le dernier tableau manipulé
              storage.setItem('lastTableau',item);
           }
           ul.appendChild(li);
        });
        }

 

        // Recharge la liste des tableaux
        let storage = window.localStorage;
        tableur.tableaux=JSON.parse(storage.getItem('tableaux'));
        if(tableur.tableaux==null){
            tableur.tableaux=[];
        }

        
        // On récupère la référence sur la div target
        let target = document.getElementById('target');
        tableur.matable = document.createElement('table');
        tableur.matable.setAttribute('class','table table-bordered');
        target.appendChild(tableur.matable);

        // On récupère une référence sur l'objet table de la page html.
        //var matable = document.getElementById(tablename);
        // On récupère les attributs de la table
        // let nbcol = matable.getAttribute("nbcol");
        // let nbrow = matable.getAttribute("nbrow");
        //console.log("nbcol="+nbcol+",nbrow="+nbrow);
        let entetes = tableur.matable.insertRow();
        // Ajoute une cellule vide pour une question d'alignement des colonnes
        let header = document.createElement('th');
        entetes.appendChild(header);
        // Une boucle sur les entetes 
        for(col=0;col<nbcol; col++ ) {
            let header = document.createElement('th');
            header.innerHTML = String.fromCharCode(65+col);
            entetes.appendChild(header);
        }
        // Les entête de colonnes
        // On boucle sur toutes les lignes et sur toutes les colonnes
        for(ligne=0;ligne<nbrow; ligne++ ) {

            let row = tableur.matable.insertRow();

            let header = document.createElement('th');
            header.innerHTML = ligne+1;
            row.appendChild(header);

            for(col=0;col<nbcol; col++ ) {
                // A chaque tour on créer une cellule avec ses attributs
               let cell = row.insertCell();
               let id = String.fromCharCode(65+col)+(ligne+1);
               cell.innerHTML = "";
               cell.id = id;
               cell.value = "";
               //cell.setAttribute('class','col-6');
               // On met la référence sur la fonction clickOnCell dans la propriété onclick de la cellule
               cell.onclick = tableur.clickOnCell; 
               // Rendre la cellule draggable
               Object.assign(cell,{
                'draggable': 'true',
                'ondragstart': tableur.dragstart_handler,
                'ondrop': tableur.drop_handler,
                'ondragover': tableur.dragover_handler,
               });
            }
        }

        // On récupère le dernier tableau manipulé
        let lastTableau = storage.getItem('lastTableau');
        if(lastTableau == null) {
            let span = document.getElementById('nameTableau');
              span.innerHTML=name;
              //storage.setItem('lastTableau',name);
        } else {
            // On affiche le nom du tableau
            let span = document.getElementById('nameTableau');
            span.innerHTML=lastTableau;
            // On renseigne le tableau courant
            tableur.loadTableau(lastTableau);
        }

    } // fin de l'init.
,
    newTableau: function() {
        console.log("newTableau");
        let target = document.getElementById('target');
        target.innerHTML="";
        let label = document.createElement('label');
        let input = document.createElement('input');
        let button = document.createElement('button');
        input.setAttribute('type','text');
        input.setAttribute('id','nom');
        input.setAttribute('value','');
        input.setAttribute('placeHolder','nom du tableau');
        label.setAttribute('for','nom');
        label.setAttribute('value','Nom du tableau :');
        button.innerHTML="Enregistrer"
        button.onclick = function () {
            tableur.tableauCourant = {};
            tableur.tableauCourant['nom']=input.value;
            tableur.tableauCourant['id']=0;
            tableur.tableauCourant.cellules = {};
            // On garde le nom du tableau dans la liste des tableaux
            tableur.tableaux.push(tableur.tableauCourant['nom']);
            // On stocke la liste des tableaux dans le localStorage
            let storage = window.localStorage;
            storage.setItem('tableaux',JSON.stringify(tableur.tableaux));
            // Initialiser le tableau
            target.innerHTML="";
            tableur.init(tableur.tableauCourant['nom'],5,8);
            // Créer le tableau vide dans le storage
            tableur.persisteTableau();
        }
        target.appendChild(input);
        target.appendChild(label);
        target.appendChild(button);
    } 
    ,
    // Cette méthode est appelée lorsqu'on clique sur une cellule
    clickOnCell: function() {
        console.log("clickclick sur "+this.id);
        this.onclick="";
        let input = document.createElement('input');
        input.setAttribute('type','text');
        // Ajoute l'id de la cellule dans le champ input
        input.setAttribute('idCell',this.id);
        // Récupère la valeur de la cellule
        let value = this.value;
        input.setAttribute('value',value);
        // On met la valeur de l'id de la cellule dans l'input
        input.setAttribute('id','input-'+this.id);
        this.innerHTML="";
        this.appendChild(input);
        input.focus();
        input.onblur = tableur.closeInput;
    }
    ,
    closeInput: function() {
        console.log("closeInput de "+this.id);
        let input = document.getElementById(this.id);
  
        let idCell = input.getAttribute('idCell');
        console.log("idCell ="+idCell);
        // récupérer la référence sur la cellule
        let cell = document.getElementById(idCell);
        // On conserve la valeur dans la cellule
        cell.value=input.value;
        // On supprime l'objet input de la cellule
        cell.removeChild(input);
        // On replace la value dans la cellule
        cell.innerHTML=cell.value;
        cell.onclick=tableur.clickOnCell;
        tableur.persistCell(cell);
    }
    ,
    persistCell: function(obj) {
        let cell = {
            'id': obj.id,
            'value': obj.value,
        }
        // On empile les objets cell dans le tableau tableauCourant['cellule']
        if(obj.value=="") {
             delete tableur.tableauCourant.cellules[obj.id];
          }
           else {
               tableur.tableauCourant.cellules[obj.id]=cell;
            }
        
        console.log(JSON.stringify(tableur.tableauCourant));
    }
    ,
    persisteTableau: function(){
        // On récupère une référence sur le localStorage
        let storage = window.localStorage;
        storage.setItem(tableur.tableauCourant.nom,JSON.stringify(tableur.tableauCourant));
        // On vérifie que le nom du tableau existe dans la liste des tableaux.
        let exists = false;
        tableur.tableaux.forEach(function(item){
            if(item == tableur.tableauCourant.nom){
                exists = true;
            }
        });
        // S'il n'existe pas on l'ajoute et on le sauvegarde
        if(! exists){
            tableur.tableaux.push(tableur.tableauCourant['nom']);
            storage.setItem('tableaux',JSON.stringify(tableur.tableaux));
        }
    }
    ,
    loadTableau: function(nom) {
        let storage = window.localStorage;
        // Transforme la chaine de caractères en objet javascript
        tableur.tableauCourant = JSON.parse(storage.getItem(nom));
        // Parcourir tous les éléments du tableau.
        for (key in tableur.tableauCourant.cellules ) {
            console.log(key+", value="+tableur.tableauCourant.cellules[key].value);
            let cell = document.getElementById(key);
            cell.innerHTML = tableur.tableauCourant.cellules[key].value;
            cell.value = tableur.tableauCourant.cellules[key].value;
        }
    }
    ,
    showTableau: function() {

    }
    ,
    // Les gestionnaires du drag and drop
    dragstart_handler: function(ev) {
        console.log('dragstart_handler');
        // console.log('target.id='+ev.target.id);
        ev.dataTransfer.setData('idSource',ev.target.id);
        ev.dataTransfer.setData('text/plain',ev.target.value);
    }
    ,
    dragover_handler: function(ev) {
        ev.preventDefault();
        console.log('dragover_handler');
    }
    ,
    drop_handler: function(ev) {
        ev.preventDefault();
        console.log('drop_handler');
        console.log('idSource='+ev.dataTransfer.getData('idSource'));
        // On recopie les données de la cellule source dans la cellule cible
        ev.target.innerHTML=ev.dataTransfer.getData('text/plain');
        ev.target.value=ev.dataTransfer.getData('text/plain');
        // On persite la cellule
        tableur.persistCell(ev.target);
    }


}
