/**
 * Gestionnaire du début de l'opération Drag n Drop
 * @param {Event} ev
 */
function dragstart_handler(ev) {
    // On ajoute l'identifiant de l'élément cible à l'objet de transfert
    ev.dataTransfer.setData("text/plain", ev.target.innerText);

    ev.dataTransfer.setData("text/html", ev.target.outerHTML);
    ev.dataTransfer.setData(
        "text/uri-list",
        ev.target.ownerDocument.location.href
    );
    // ev.target représente la balise p avec l'id "p1"
    ev.dataTransfer.setData("id", ev.target.id);

    // On crée une image qu'on utilise pour le déplacement

    var img = new Image();
    img.src = "move_icon.png";
    ev.dataTransfer.setDragImage(img, 10, 10);

    // On défini l'effet : copy / move / link
    ev.dataTransfer.dropEffect = "copy";
}

/**
 * Gestionnaire de la fin de l'opération drag n drop
 * @param {Event} ev
 */
function dragover_handler(ev) {
    ev.preventDefault();
    ev.dataTransfer.dropEffect = "move";
}

/**
 * Gestionnaire du "dépôt"
 * @param {Event} ev
 */
function drop_handler(ev) {
    ev.preventDefault();
    // On récupère l'identifiant de la cible et on ajoute l'élément déplacé au DOM de la cible
    var data = ev.dataTransfer.getData("id");
    // ev.target représente la balise div avec l'id "target"
    // document.getElementById(data) donne une référence sur l'id "p1"
    ev.target.appendChild(document.getElementById(data));
    // console.log(ev);
}
