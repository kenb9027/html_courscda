const mytable = document.querySelector("#mytable");

function createTable() {

  mytable.innerHTML = " ";

  const nbCol = mytable.getAttribute("nbcol");
  const nbRow = mytable.getAttribute("nbRow");
  let theaderHTML = `<thead><tr>`;
  theaderHTML += `<th scope="col" class="text-center table-primary" >#</th>`;
  for (var i = 1; i <= nbCol; i++) {
    theaderHTML += `<th scope="col" class="text-center table-primary" >Colonne ${i}</th>`;
  }
  theaderHTML += `</tr></thead>`;

  mytable.innerHTML += theaderHTML;

  let tbodyHTML = `<tbody>`;
  for (var j = 1; j <= nbRow; j++) {
    tbodyHTML += `<tr>`;
    tbodyHTML += `<th class="text-center table-primary" scope="row">Ligne ${j}</th>`;
    for (var k = 1; k <= nbCol; k++) {
      let jk = j + "" + k;
      tbodyHTML += `<td id="${jk}" class="text-center" > `;
      tbodyHTML += `</td>`;
    }
    tbodyHTML += `</tr>`;
  }

  tbodyHTML += `</tbody>`;

  mytable.innerHTML += tbodyHTML;
}

function addInputIn(cellId, inValue) {
  //   console.log("click on cell " + cellId);
  let inputId = "inp-" + cellId;
  let cell = document.getElementById(cellId);
  if (cell.innerHTML == " ") {
      let input = `<input type="text" id=${inputId} class="text-center form-control" `;
    //   console.log(inValue);
    if (inValue !==  undefined) {
        input += ` value="${inValue}" `;
      }
      input += `  />`;
      
      cell.innerHTML = input;
      
  }

  let input = document.getElementById(inputId);

  if (input) {
    input.addEventListener("focusout", (event) => {
      //   console.log("click on cell " + cellId + " : " + input.value);

      let spanId = "sp" + cellId;
      let spanVal = `<span id="${spanId}" class="spanVal text-success fw-bold" onclick="changeSpanValue(${spanId})" >${input.value}</span>`;
      if (input.value) {
        cell.innerHTML = spanVal;
      } else {
        cell.innerHTML = " ";
      }
    });
  }
}

function changeSpanValue(span) {
    // console.log(span);
    let spanId = span.getAttribute("id");
    let spanValue = span.innerHTML;
    // console.log(spanValue);
  let cellId = spanId.slice(2);
  //   console.log(cellId);
  let cell = document.getElementById(cellId);
  cell.innerHTML = " ";
  addInputIn(cellId, spanValue);
}




document.addEventListener("DOMContentLoaded", function () {
  createTable();

  let cells = document.querySelectorAll("td");
  //   let inputs = document.querySelectorAll("input");
  //   let spans = document.querySelectorAll(".spanVal");
  cells.forEach((element) => {
    let cellId = element.getAttribute("id");
    element.addEventListener("click", (e) => {
      addInputIn(cellId);
    });
  });
});
