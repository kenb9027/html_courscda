let ws = '';

var btnDeConnecte = document.getElementById("btnDeConnecte");
btnDeConnecte.disabled = true;

var btnEnvoi = document.getElementById("btnEnvoi");
btnEnvoi.disabled = true;

function connecte() {
    
    let username = document.getElementById("username").value;
    ws = new WebSocket("ws://192.168.43.178:1299/chat/" + username);

    var btnConnecte = document.getElementById("btnConnecte");
    var btnDeConnecte = document.getElementById("btnDeConnecte");
    var btnEnvoi = document.getElementById("btnEnvoi");

    btnConnecte.disabled = true;
    btnDeConnecte.disabled = false;
    
    ws.onopen = function (event) {
        console.log('data : ' + JSON.stringify(event.data));
        
        let message = document.getElementById("message");
        message.innerHTML = "Connexion établie : Bonjour " + username;
        btnEnvoi.disabled = false;

    };
    ws.onerror = function (event) {
        
    };

    ws.onmessage = function (message) {
        console.log("onmessage: message={" + message.data + "}");
        
        let user = getUserFromMessage(message.data);
        console.log('user: '+user);

        let listMessage = document.getElementById("listMessage");
        let p = document.createElement('p');
        p.innerHTML = message.data;
        if (user.trim() == username.trim()) {
            p.setAttribute('class', 'sendMess');
        }
        else {
            p.setAttribute('class', 'receiveMess');
        }
        listMessage.appendChild(p);
        listMessage.scrollTop = listMessage.scrollHeight;

    };

    ws.onclose = function (event) {
        console.log("Fin de connexion");
    };
}

function deconnecte() {
    if (ws != null) {
        ws.close();
        let message = document.getElementById("message");
        message.innerHTML = "Connexion terminée...";

        var btnConnecte = document.getElementById("btnConnecte");
        var btnDeConnecte = document.getElementById("btnDeConnecte");
        var btnEnvoi = document.getElementById("btnEnvoi");
    
        btnConnecte.disabled = false;
        btnDeConnecte.disabled = true;
        btnEnvoi.disabled = true;

        username = "";
    }
}

function send() {
    let data = JSON.stringify({
        'user': document.getElementById('username').value ,
        'message' : document.getElementById('inputMessage').value
    })

    console.log('send: ' + data);
	ws.send(data); 
    let message = document.getElementById('message');
    message.innerHTML = 'message envoyé...'
}
function getUserFromMessage(data){
    let tab = data.split("-");

    let user = tab[0].replace('/\s+g', "");
    return user;
}
